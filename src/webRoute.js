const homeController = require("./controllers/homeController");
const gameController = require("./controllers/gameController");
const loginController = require("./controllers/loginController");
const userGameController = require("./controllers/userGameController");


app.get("/", homeController.home);
app.get("/game", gameController.game);
app.get("/login", loginController.login);
app.post("/login", loginController.loginpost);
app.get("/user/index", userGameController.userview);
app.get("/user/new", userGameController.usernew);
app.post("/user/add", userGameController.adduser);
app.get("/user/edit/:id", userGameController.updateuser);
app.post("/user/edit/:id", userGameController.edituser);
app.get("/user/delete/:id", userGameController.deleteuser);
app.get("/user/detail/:id", userGameController.detailuser);
