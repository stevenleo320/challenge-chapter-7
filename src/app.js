const express = require("express");
const session = require("express-session");
const passport = require("passport");
const path = require("path");
const routeApi = require("./route-api");

// YANG LAMA BELUM DIIMPORT JADI BAKAL ERROR
// const logger = require("pino-http");
// const dotenv = require("dotenv");

const homeController = require("./controllers/homeController");
const gameController = require("./controllers/gameController");
const loginController = require("./controllers/loginController");
const userGameController = require("./controllers/userGameController");

const server = (app) => {
  app.set("views", path.join(__dirname, "views"));
  app.set("view engine", "ejs");

  app.use(express.urlencoded({ extended: true }));
  app.use(express.json());
  app.use(express.static("public"));
  
  app.use(session({
    secret: "secret",
    resave: false,
    saveUninitialized: false
  }));
  app.use(passport.initialize());
  app.use(passport.session());

  // FOR API AND CHALLENGE CHAPTER 7
  app.use("/api/v1", routeApi);

  // WEB ROUTE
  app.get("/", homeController.home);
  app.get("/game", gameController.game);
  app.get("/login", loginController.login);
  app.post("/login", loginController.loginpost);
  app.get("/user/index", userGameController.userview);
  app.get("/user/new", userGameController.usernew);
  app.post("/user/add", userGameController.adduser);
  app.get("/user/edit/:id", userGameController.updateuser);
  app.post("/user/edit/:id", userGameController.edituser);
  app.get("/user/delete/:id", userGameController.deleteuser);
  app.get("/user/detail/:id", userGameController.detailuser);

  return app;
}

module.exports = server;