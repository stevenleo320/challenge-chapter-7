const { UserGame, UserGameBiodata, UserGameHistory } = require("../models");

module.exports = {
    userview: async(req,res) => {

        const data = await UserGame.findAll({
            include: [
                {
                    model: UserGameBiodata
                },
                {
                    model: UserGameHistory
                }
            ]
        });
        res.render("user/index",{
            data,
            number : 1,
            status : 200,
            pageData: {
                pageTitle: "LIST USER PAGE",
                pageDescription: "LIST USER DESCRIPTION"
            }
        });   
    },
    usernew: async(req,res) => {
        res.render("user/new",{
            status : 200,
            pageData: {
                pageTitle: "ADD USER PAGE",
                pageDescription: "ADD USER DESCRIPTION"
            }
        });    
    },
    adduser: async (req,res) => {
        const { username, password } = req.body;
    
        if(username && password) {
            const i_data = await UserGame.create({username,password});
    
            res.redirect("/user/index");
        } else {
            res.render("user/new", {
                status : 500
            });
        }
    },
    updateuser: async (req,res) => {
        const data = await UserGame.findOne({
            where : { 
                id : req.params.id
            }
        });
    
        res.render("user/edit", {
            data,
            pageData: {
                pageTitle: "UPDATE USER PAGE",
                pageDescription: "UPDATE USER DESCRIPTION"
            }
        });
    },
    edituser: async (req,res) => {
        const { body } = req;
    
        const data = await UserGame.update(body,{
            where : {
                id : req.params.id
            }
        });
    
        res.redirect("/user/index");
    },
    deleteuser: async (req,res) => {
        data_3 = await UserGameHistory.destroy({
            where: {
                userId : req.params.id
            }
        });
    
        data_2 = await UserGameBiodata.destroy({
            where: {
                userId : req.params.id
            }
        });
    
        data_1 = await UserGame.destroy({
            where: {
                id : req.params.id
            }
        });
                
        res.redirect("/user/index");
    },
    detailuser: async (req,res) => {
        biodata = await UserGameBiodata.findOne({
            where: {
                userId : req.params.id
            }
        });
    
        history = await UserGameHistory.findAll({
            where: {
                userId : req.params.id
            }
        });
    
        res.render("user/detail",{
            biodata,
            b_number : 1,
            history,
            h_number : 1,
            pageData: {
                pageTitle: "DETAIL USER PAGE",
                pageDescription: "DETAIL USER DESCRIPTION"
            }
        });
    }
}