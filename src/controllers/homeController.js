
module.exports = {
    home: (req,res) => {
        try {
            res.render("home", {
                status : 0,
                pageData: {
                    pageTitle: "HOME PAGE",
                    pageDescription: "HOME DESCRIPTION"
                }
            });
        } catch (error) {
            res.render("Internal Problem Server");
        }
    }
}