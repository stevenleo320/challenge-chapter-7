module.exports = {
    game: (req,res) => {
        try {
            res.render("game", {
                status : 0,
                pageData: {
                    pageTitle: "GAME PAGE",
                    pageDescription: "GAME DESCRIPTION"
                }
            });
        } catch (error) {
            res.render("Internal Problem Server");
        }
    }
}