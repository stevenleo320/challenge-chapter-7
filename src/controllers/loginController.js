const { UserGame, UserGameBiodata, UserGameHistory } = require("../models");
const url = require('url');
const jwt = require("jsonwebtoken");
const passport = require("passport");
const { Strategy: JWTStrategy, ExtractJwt } = require("passport-jwt");
const LocalStrategy = require("passport-local");  

// CARA PAKE PASSPORT USE DAN ALLOWED RULES DI CONTROLLER GIMANA ? 
// const allowedRoles = (roles) => (req, res, next) => {
//     if (roles.includes(req.user.role)) {
//       next();
//     } else {
//       res.status(401).send("Unauthorized");
//     }
// };

// passport.use(new JWTStrategy({
//     jwtFromRequest: ExtractJwt.fromHeader("authorization"),
//     secretOrKey: "secret"
// }, async (payload,done) => {
//     try {
//         if(new Date() > new Date(payload.accessTokenExpiredAt)) {
//             throw new Error('Access token expired');
//         }

//         // ambil data dari res.json pas login 
//         const user = await UserGame.findByPk(payload.id);

//         return done(null, user);
    
//     } catch (error) {
//         return done(null, false, { message: error.message });
//     }
// }));

module.exports = {
    login: (req,res) => {
        try {
            res.render("login", {
                status : 200,
                pageData: {
                    pageTitle: "LOGIN PAGE",
                    pageDescription: "LOGIN DESCRIPTION"
                }
            });
        } catch (error) {
            res.json("Internal Problem Server");
        }
    },
    loginpost: async (req,res) => {

        const { username, password } = req.body;
    
        if(username && password) {
    
            const v_user = await UserGame.findOne({
                where: {
                    username: req.body.username,
                    password: req.body.password
                }
            });
    
            if(v_user.username === req.body.username && v_user.password === req.body.password) {
                const data = await UserGame.findAll({
                    // where: {
                    //     id : v_user.id
                    // },
                    include: [
                        {
                            model: UserGameBiodata
                        },
                        {
                            model: UserGameHistory
                        }
                    ]
                });
                res.render("user/index",{
                    data,
                    number : 1,
                    status : 200,
                    pageData: {
                        pageTitle: "LIST USER PAGE",
                        pageDescription: "LIST USER DESCRIPTION"
                    }
                });
            } 
            else {
                res.render("login", {
                    status : 500
                });
            }
        } else {
            res.render("login", {
                status : 412
            });
        }
    
    }
}