const express = require("express");
const route = express.Router();
const { UserGame,GameRoom,Fight } = require("./models");
const jwt = require("jsonwebtoken");
const passport = require("passport");
const { Strategy: JWTStrategy, ExtractJwt } = require("passport-jwt");
const LocalStrategy = require("passport-local");

const dataSuit = [];

// REGISTER 
route.post("/register", async(req, res) => {
    try {
        const { name, username, password, role } = req.body;

        if(name && username && password && role) {
            const newuser = await UserGame.register({name, username, password, role});
            
            if(newuser) {
                res.json(newuser);
            }
            else {
                res.json("Fail to register !");
            }
        } 
        else {
            res.json("Input can't be null");
        }

      } catch (error) {
        res.json("Error happen when you register !");
      }
});

// LOGIN
// Validasi buat cek apakah token sudah expired / belum
passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJwt.fromHeader("authorization"),
    secretOrKey: "secret"
}, async (payload,done) => {
    try {
        if(new Date() > new Date(payload.accessTokenExpiredAt)) {
            throw new Error('Access token expired');
        }

        // ambil data dari res.json pas login 
        const user = await UserGame.findByPk(payload.id);

        return done(null, user);
    
    } catch (error) {
        return done(null, false, { message: error.message });
    }
}));

route.post("/login", async (req, res, next) => {
    try {
      const user = await UserGame.authenticate(req.body);
      console.log(user);
      const today = new Date();
      const accessTokenExpiredAt = today.setHours(today.getHours() + 2);
      res.json({
        // untuk menampilkan accessToken -> kalo di jwt.io bakal menampilkan id,username,accessTokenExpiredAt
        // dan accessTokenExpiredAt
        // return user dari model menghasilkan data dari findOne({ username }); 
        accessToken: jwt.sign({
          id: user.id,
          name: user.name,
          username: user.username,
          role: user.role,
          accessTokenExpiredAt
        }, "secret"),
        accessTokenExpiredAt
      });
    } catch (error) {
      next(error);
    }
});

const allowedRoles = (roles) => (req, res, next) => {
    if (roles.includes(req.user.role)) {
      next();
    } else {
      res.status(401).send("Unauthorized");
    }
};

// CUMAN PLAYER 1 YANG DIINSERT KARENA INI YANG BUAT ROOM ADALAH PLAYER 1
// ONLYL PLAYER CAN CREATE ROOM
route.post("/create-room", passport.authenticate("jwt", { session: false }),
  allowedRoles([ "PlayerUser" ]),
  async (req, res) => {
    const { room_name, status } = req.body;
    const player1  = req.user.id;
    const createdBy = req.user.name;
    
    const create_room = await GameRoom.create({room_name,player1,status,createdBy});

    if(create_room) {
        res.json("Room Successfully created");
    }
    else {
        res.json("Failed to create room");
    }
});

route.post("/fight/insert", passport.authenticate("jwt", { session: false }),
allowedRoles([ "PlayerUser", "SuperAdmin" ]), async(req,res) => {

    const suit = req.body.suit;

    console.log(suit.toString());

    await Fight.create({
        suit_player2: suit
    });

    // if(a.toString) {

        res.json("success")
    // }
    // else {
    //     res.json("Fail");
    // }
    
});

// PLAYER 1 FIGHT/PLAYER1/:ROOM_NAME?P1=R

// PLAYER 2 JOIN & FIGHT WITH PLAYER 1
// PARAM ROOMNAME
// BODY SUIT = ROCK,PAPER,SCISSOR
route.post("/fight/:room_name", passport.authenticate("jwt", { session: false }),
  allowedRoles([ "PlayerUser", "SuperAdmin" ]),
  async (req, res) => {

    // BODY ADALAH TEMPAT KITA ISI DATA YANG INGIN DI POST
    const suit  = req.body.suit;
    const player = req.user.id;
    
     // CEK APAKAH ROOM ADA ATAU TIDAK
    const checkRoom = await GameRoom.findOne({
        where: { 
            room_name: req.params.room_name
        }
    });

    // CEK INPUT ARRAY TIDAK
    if(!Array.isArray(suit)) {
        res.status(500).send("Please input your choice inside Array");
    }

    // CHECK LENGTH ARRAY 3 APA TIDAK
    if(suit.length != 3) {
        res.status(500).send("Please input 3 Choice Inside Array -> ['R','P','S'] ");
    }

    // CEK P2 UDAH JOIN ATAU BELOM
    if(checkRoom) {
        // KALO PLAYER SUDAH JOIN

        if(checkRoom.player2 !== null) 
        {
            // CEK APAKAH INI P1
            // CEK ID YANG LOGIN DENGAN ID PLAYER 1 DI TABLE GAME ROOM
            if(checkRoom.player1 === player) {
                const p1 = await Fight.update(
                {
                    suit_player1 : suit
                },{
                    where: {
                        roomId: checkRoom.id
                    }
                });

                if(p1) {
                    let p1_score = 0;
                    let p2_score = 0;

                    const p_suit = await Fight.findOne({
                        where: {
                            roomId: checkRoom.id
                        }
                    }); 
                    
                    for(const index in p_suit.suit_player2) {
                        const p1_choice = p_suit.suit_player1[index];
                        const p2_choice = p_suit.suit_player2[index];

                        // BUAT YANG DRAW
                        if(p1_choice === p2_choice) {
                            p1_score +=1;
                            p2_score +=1;
                        }
                        else if(p1_choice === 'R' && p2_choice === 'P') {
                            p2_score +=1;                            
                        }
                        else if(p1_choice === 'R' && p2_choice === 'S') {
                            p1_score +=1;
                        }
                        else if(p1_choice === 'P' && p2_choice === 'R') {
                            p1_score +=1;
                        }
                        else if(p1_choice === 'P' && p2_choice === 'S') {
                            p2_score +=1;
                        }
                        else if(p1_choice === 'S' && p2_choice === 'R') {
                            p1_score +=1;
                        }
                        else if(p1_choice === 'S' && p2_choice === 'P') {
                            p2_score +=1;
                        }
                        else {
                            res.status(500).send("Something wrong with function choose winner");
                        }
                    }

                    // 0 DRAW, SISANYA ID DARI WINNER
                    if(p1_score > p2_score) {
                        
                        await Fight.update({
                            winner: checkRoom.player1
                        },{
                            where: {
                                roomId: checkRoom.id
                            }
                        });

                        await GameRoom.update({
                            status: "P1 WIN"                            
                        },{
                            where: {
                                id: checkRoom.id
                            }
                        });

                        res.status(200).send("PLAYER 1 WIN");
                    }
                    else if(p1_score < p2_score) {
                        await Fight.update({
                            winner: checkRoom.player2
                        },{
                            where: {
                                roomId: checkRoom.id
                            }
                        });

                        await GameRoom.update({
                            status: "P2 WIN"                            
                        },{
                            where: {
                                id: checkRoom.id
                            }
                        });

                        res.status(200).send("PLAYER 2 WIN");
                    }
                    else if(p1_score == p2_score) {
                        // WINNER NULL == DRAW <- KARENA FK JADI GABISA DI SET 0

                        await GameRoom.update({
                            status: "DRAW"                            
                        },{
                            where: {
                                id: checkRoom.id
                            }
                        });
    
                        res.status(200).send("DRAW");
                    }
                    else {
                        res.status(200).send("Something wrong happen while calculating score !");
                    }
                }
                else {
                    res.status(404).send("P1 Fail input suit !");                    
                }
            // CASE KALO ROOM ADA DAN P2 INPUT PADAHAL P1 BELUM INPUT
            } else {
                res.status(400).send("Please wait response from P1 before submit another response !");
            }
        } 
        // KALO PLAYER 2 BELUM JOIN
        else if(checkRoom.player2 === null) 
        {
            const i_player2 = await GameRoom.update(
            {
                player2 : player,
                status: "In Game"
            },{
                where: {
                    room_name: req.params.room_name
                }
            });

            // SETELAH PLAYER 2 JOIN ROOM
            if(i_player2) {
                const p2 = await Fight.create({
                    suit_player2: suit,
                    roomId: checkRoom.id
                });

                if(p2) {
                    res.status(400).send("Waiting response from player 1");
                }
                else {
                    res.status(500).send("Fail to create fight list");                    
                }
            }
            else {
                res.status(400).send("Player fail to join room !");
            }
        }
    }
    // KALO DI CEK ROOM TIDAK ADA
    else {
        res.status(503).send("Room doesn't exist !");
    }

});



module.exports = route;