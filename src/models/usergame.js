'use strict';
const {
  Model
} = require('sequelize');
const { encrypt, comparePassword } = require('../utils/common');

module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      UserGame.hasMany(models.GameRoom,{
        foreignKey: "player1"
      });
      UserGame.hasMany(models.GameRoom,{
        foreignKey: "player2"
      });
      UserGame.hasMany(models.Fight,{
        foreignKey: "winner"
      });
      UserGame.hasOne(models.UserGameBiodata, {
        foreignKey : "userId"
      });
      UserGame.hasMany(models.UserGameHistory,{
        foreignKey: "userId"
      });

    }

    static register({ name, username, password, role }) {
      return this.create({ name, username, password: encrypt(password), role });
    }

    static async authenticate({ username, password }) {
      const user = await this.findOne({ where: { username } });
      if (!user) throw new Error("User not found");

      console.log(user.password);
      const isPasswordValid = comparePassword(password, user.password);
      if (!isPasswordValid) throw new Error("User not found");

      return user;
    }
  }
  UserGame.init({
    name: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserGame',
  });
  return UserGame;
};