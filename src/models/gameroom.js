'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GameRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      GameRoom.belongsTo(models.UserGame,{
        foreignKey: "player1"
      });
      GameRoom.belongsTo(models.UserGame,{
        foreignKey: "player2"
      });
      GameRoom.hasMany(models.Fight,{
        foreignKey: "id"
      });
    }
  }
  GameRoom.init({
    room_name: DataTypes.INTEGER,
    player1: DataTypes.INTEGER,
    player2: DataTypes.INTEGER,
    status: DataTypes.STRING,
    createdBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'GameRoom',
  });
  return GameRoom;
};