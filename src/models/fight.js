'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Fight extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Fight.belongsTo(models.UserGame,{
        foreignKey: "winner"
      });

      Fight.belongsTo(models.GameRoom,{
        foreignKey: "roomId"
      });

    }
  }
  Fight.init({
    roomId: DataTypes.INTEGER,
    suit_player1: DataTypes.ARRAY(DataTypes.ENUM(["R", "P", "S"])),
    suit_player2: DataTypes.ARRAY(DataTypes.ENUM(["R", "P", "S"])),
    winner: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Fight',
  });
  return Fight;
};