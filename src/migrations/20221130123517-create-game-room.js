'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('GameRooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      room_name: {
        type: Sequelize.INTEGER
      },
      player1: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName : 'UserGames',
            schema : 'public'
          },
          key : 'id'
        }
      },
      player2: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName : 'UserGames',
            schema : 'public'
          },
          key : 'id'
        }
      },
      status: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('GameRooms');
  }
};