'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Fights', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      roomId: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName : 'GameRooms',
            schema : 'public'
          },
          key : 'id'
        }
      },
      suit_player1: {
        type: Sequelize.ARRAY(Sequelize.ENUM(["R", "P", "S"]))
      },
      suit_player2: {
        type: Sequelize.ARRAY(Sequelize.ENUM(["R", "P", "S"]))
      },
      winner: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName : 'UserGames',
            schema : 'public'
          },
          key : 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Fights');
  }
};