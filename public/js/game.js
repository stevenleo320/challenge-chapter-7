// 1. Abstract (done)
// 2. Inheritance (done)
// 3. Encapsulation
// 4. Polymorphism
// 5. DOM

class User {
    _win = "win";
    _lose = "lose";
    _draw = "draw";

    constructor() {
        if(this.constructor === User) {
            throw new Error("Cannot instantiate from Abstract Class");
        }
    }

    _drawUser() {
        throw new Error("Fungsi ini belum diimplementasikan");
    }

    _winUser(){
        throw new Error("Fungsi ini belum diimplementasikan");
    }

    _loseUser() {
        throw new Error("Fungsi ini belum diimplementasikan");
    }

    _drawComp() {
        throw new Error("Fungsi ini belum diimplementasikan");
    }

    _winComp(){
        throw new Error("Fungsi ini belum diimplementasikan");
    }

    _loseComp() {
        throw new Error("Fungsi ini belum diimplementasikan");
    }

    showChoice() {
        throw new Error("Fungsi ini belum diimplementasikan");
    }
}

class Player extends User {
    _userChoice = "";

    constructor(props) {
        super();
        this._playerName = Player.name;
    }

    drawUser() {
        console.log(`DRAW`);
        annoucement.style.fontSize = "18px";  
        annoucement.style.color = "white";
        annoucement.innerHTML ="DRAW";
    }

    winUser() {
        console.log(`PLAYER 1 WIN`);
        annoucement.style.fontSize = "30px";  
        annoucement.style.color = "white";
        annoucement.innerHTML = "PLAYER 1 WIN";
    }

    loseUser() {
        console.log(`COMP WIN`);
        annoucement.style.fontSize = "30px";  
        annoucement.style.color = "white";
        annoucement.innerHTML = "COM WIN";
    }

    showChoice(value) {
        console.log(`${this._playerName} choice : ${value}`);
    }

}

class Computer extends User {
    _compChoice = "";

    constructor() {
        super();
        this._computerName = Computer.name;
    }

    drawComp() {
        versus.style.backgroundColor = "#035B0C";  
        versus.style.transform = "rotate(-28.87deg)";  
    }

    winComp() {
        versus.style.backgroundColor = "#4C9654";
        versus.style.transform = "rotate(-28.87deg)";  

    }

    loseComp() {
        versus.style.backgroundColor = "#4C9654";
        versus.style.transform = "rotate(-28.87deg)";  
    }

    compChoice(jenisSuit) {
        const compSuit = jenisSuit[Math.floor(Math.random() * jenisSuit.length)];
        console.log(`${this._computerName} choice : ${compSuit}`);
        return compSuit;
    }

    refresh() {
        refresh.classList.remove("block");
    }

    blockButton() {
        user_r.classList.add("block");
        user_s.classList.add("block");
        user_p.classList.add("block");
        
        comp_r.classList.add("block");
        comp_s.classList.add("block");
        comp_p.classList.add("block");
    }

}

const user_r = document.getElementById("user-r");
const user_s = document.getElementById("user-s");
const user_p = document.getElementById("user-p");

const comp_r = document.getElementById("comp-r");
const comp_s = document.getElementById("comp-s");
const comp_p = document.getElementById("comp-p");

const versus = document.getElementById("versus");
const annoucement = document.getElementById("text-annoucement");
const refresh = document.getElementById("btn-refresh");

const player = new Player;
const comp = new Computer;

const jenisSuit = ['batu','gunting','kertas'];

function suit(value) {
    player.showChoice(value);

    if(value === "batu") {
        user_r.style.backgroundColor = "#C4C4C4";
            let hasilSuit = comp.compChoice(jenisSuit);

        if(hasilSuit === "batu") {
            comp_r.style.backgroundColor = "#C4C4C4";
            player.drawUser();
            comp.drawComp();
        }
        else if(hasilSuit === "gunting") {
            comp_s.style.backgroundColor = "#C4C4C4";
            player.winUser();
            comp.loseComp();
        }
        else {
            comp_p.style.backgroundColor = "#C4C4C4";
            player.loseUser();
            comp.winComp();
        }

        // Add class block di button gunting, batu kertas
        comp.blockButton();
        // Remove class block di button refresh
        comp.refresh();
    }
    else if(value === "gunting") {
        user_s.style.backgroundColor = "#C4C4C4";
        let hasilSuit = comp.compChoice(jenisSuit);
        if(hasilSuit === "gunting") {
            comp_s.style.backgroundColor = "#C4C4C4";
            player.drawUser();
            comp.drawComp();
        }
        else if(hasilSuit === "kertas") {
            comp_p.style.backgroundColor = "#C4C4C4";
            player.winUser();
            comp.loseComp();
        }
        else {
            comp_r.style.backgroundColor = "#C4C4C4";
            player.loseUser();
            comp.winComp();
        }
        
        comp.blockButton();
        comp.refresh();

    }
    else {
        user_p.style.backgroundColor = "#C4C4C4";
        let hasilSuit = comp.compChoice(jenisSuit);

        if(hasilSuit === "kertas") {
            comp_p.style.backgroundColor = "#C4C4C4";
            player.drawUser();
            comp.drawComp();
        }
        else if(hasilSuit === "batu") {
            comp_r.style.backgroundColor = "#C4C4C4";
            player.winUser();
            comp.loseComp();
        }
        else {
            comp_s.style.backgroundColor = "#C4C4C4";
            player.loseUser();
            comp.winComp();
        }

        comp.blockButton();
        comp.refresh();
    }
}

refresh.addEventListener("click",function(){
    refresh.classList.add("block");

    user_r.classList.remove("block");
    user_s.classList.remove("block");
    user_p.classList.remove("block");
    
    comp_r.classList.remove("block");
    comp_s.classList.remove("block");
    comp_p.classList.remove("block");

    user_r.removeAttribute("style");
    user_s.removeAttribute("style");
    user_p.removeAttribute("style");

    comp_r.removeAttribute("style");
    comp_s.removeAttribute("style");
    comp_p.removeAttribute("style");

    annoucement.innerHTML = "VS";
    annoucement.removeAttribute("style");
    versus.removeAttribute("style");
});




