## Challenge Chapter 7

## Model

Create User Game Model
```
npx sequelize-cli model:generate --name UserGame --attributes username:string,password:string
```

Create User Game Biodata Model
```
npx sequelize-cli model:generate --name UserGameBiodata --attributes name:string,email:string,country:string,userId:integer
```

Create User Game History Model
```
npx sequelize-cli model:generate --name UserGameHistory --attributes game_name:string,playing_time:integer,score:integer,userId:integer,biodataId:integer
```

Create Game Room Model
```
npx sequelize-cli model:generate --name GameRoom --attributes room_name:integer,player1:integer,player2:integer,status:string
```
Create Fight Model
```
npx sequelize-cli model:generate --name Fight --attributes roomId:integer,suit_player1:array,suit_player2:array,winner:integer
```

## Migration

```
npx sequelize-cli db:create
```

```
npx sequelize-cli db:migrate
```

```
npx sequelize-cli db:migrate:undo
```

## Seeder

```
npx sequelize-cli db:seed:all
```

```
npx sequelize-cli db:seed:undo
```













